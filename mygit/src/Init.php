<?php


class Init
{
    public static function go($dir)
    {
        $appDir = $dir . '/mygit/app';
        if (!file_exists($appDir) && !is_dir($appDir)) {
            mkdir($appDir);
        }
        if (!file_exists($appDir . '/' . 'dirs.json')) {
            file_put_contents($appDir . '/' . 'dirs.json', '{}');
        }
        if (!file_exists($appDir . '/' . 'files.json')) {
            file_put_contents($appDir . '/' . 'files.json', '{}');
        }
        if (!file_exists($appDir . '/' . '.gitignore')) {
            file_put_contents($appDir . '/' . '.gitignore', '*');
        }
        echo "\napp created\nedit mygit/app/dirs.json\n";
    }
}