<?php


class MyGit
{
    private $dirs;
    private $files;
    private $updatedFiles = [];
    private $appDir;
    private $workingDir;

    function __construct($dir)
    {
        $this->workingDir = $dir;
        $this->appDir = $dir . "/mygit/app";
    }

    function go()
    {
        $this->readDirs();
        $this->readFilesList();
        $this->scanDirs();
        $this->writeZip();
        $this->mergeFilesList();
        $this->writeFilesList();
    }

    function readDirs()
    {
        $this->dirs = $this->readJson( 'dirs.json');
        if (!$this->dirs) {
            die("dirs.json is empty");
        }
        echo "folders to scan:\n";
        $this->printDirsTree($this->dirs);
        echo "\n";
    }

    protected function printDirsTree($dirs, $depth = 0) {
        $depth++;
        foreach ($dirs as $k => $v) {
            echo str_repeat("  ", $depth - 1) . $k . "/\n";
            foreach ($v as $ik => $iv) {
                echo str_repeat("  ", $depth) . $ik . "/\n";
                $this->printDirsTree($iv,$depth + 1);
            }
        }
    }

    function readFilesList()
    {
        $this->files = $this->readJson( 'files.json');
    }

    protected function readJson($filename)
    {
        if (!file_exists($this->appDir . '/'. $filename)) {
            die("no $filename");
        }
        $encoded = json_decode(file_get_contents($this->appDir . '/'. $filename), true);
        if (!is_array($encoded)) {
            die("wrong $filename format");
        }
        return $encoded;
    }

    function scanDirs()
    {
        $this->scanDir($this->dirs);
        if (!$this->updatedFiles) {
            die ("no file updated");
        }
        echo sprintf("%d files detected", count($this->updatedFiles));
    }

    protected function scanDir($dirs, $path = "")
    {
        foreach ($dirs as $dir => $content) {
            if (!$content && is_dir($path . $dir)) {
                $content = scandir($path . $dir);
                unset($content[0], $content[1]);
                $content = array_fill_keys($content, []);
            }
            if (is_dir($path  . $dir)) {
                $this->scanDir($content, $path . $dir . "/");
            } else {
                $file = $path . $dir;
                $filetime = filemtime($file);
                if (isset($this->files[$file]) && $this->files[$file] < $filetime) {
                    $this->updatedFiles[$file] = $filetime;
                }
            }
        }
    }

    function mergeFilesList()
    {
        foreach ($this->updatedFiles as $k => $v) {
            $this->files[$k] = $v;
        }
    }

    function writeFilesList()
    {
        file_put_contents($this->appDir . '/files.json', json_encode($this->files));
    }

    function writeZip()
    {
        $count = 0;
        $zip = new ZipArchive();
        $zipFile = 'up' . date('YmdHis') . '.zip';
        $zip->open("./{$zipFile}", ZipArchive::CREATE);
        foreach ($this->updatedFiles as $file => $time) {
            if ($zip->addFile($file)) {
                $count++;
            } else {
                echo "$file wasn't added to zip\n";
            }
        }
        $zip->close();

        echo "$count files added to $zipFile\n";
        if ($count != count($this->updatedFiles)) {
            echo (count($this->updatedFiles) - $count) . "lost\n";
        }
    }
}
