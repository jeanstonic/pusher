<?php

include __DIR__ . '/mygit/src/MyGit.php';
include __DIR__ . '/mygit/src/Init.php';


$appDir = __DIR__ . '/mygit/app';
if (!file_exists($appDir) && !is_dir($appDir)) {
    Init::go(__DIR__);
    die;
}


$myGit = new MyGit(__DIR__);
$myGit->go();